from django.urls import path

from .views import list_receipts

urlpatterns = [
    path("", list_receipts, name="home"),
]
