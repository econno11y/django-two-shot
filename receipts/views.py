from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from .models import Receipt

# Create your views here.


@login_required(login_url="login")
def list_receipts(request):
    receipts = Receipt.objects.all().filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/list.html", context)
